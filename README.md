
## Frappe Manual

Frappe Framework Manual for Enterprise Teams.

[Documentation](https://imtec.gitlab.io/help-doc)

## Contribute

Clone and switch to directory

```shell
git clone https://gitlab.com/imtec/help-doc.git
cd help-doc
```

Re-Open in devcontainer

```shell
cp -R devcontainer-example .devcontainer
```

Setup and start help-doc

```shell
python -m venv env
. ./env/bin/activate
pip install -r requirements.txt
mkdocs serve
```

Open http://127.0.0.1:8000/help-doc in browser.
