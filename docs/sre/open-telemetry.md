
# APM with track and trace with Open-Telemetry

Frappe has following python processes

- Gunicorn
- Python RQ Worker
- Bench Schedular

To have a OpenTelemetry wrapper with common base instrument dependencies we have added them as a part of `Frappe Utils`. With [Frappe Utils](https://gitlab.com/castlecraft/frappe_utils) installed you can follow steps below to have respective processes wrapped with `opentelemetry-instrument`.

For any `opentelemetry-instrument` config variable parameters kindly use environment variables, refer [docs](https://opentelemetry.io/docs/languages/sdk-configuration/general/#otel_resource_attributes).

**Frappe Utils `1.24.0`/`2.17.0` or above is needed for OpenTelemetry dependencies**

> Below steps are with consideration that we have work directory set to `$FRAPPE-BENCH/sites`

## Gunicorn

```sh
$ ../env/bin/opentelemetry-instrument \
    ../env/bin/gunicorn --bind=0.0.0.0:8000 --threads=4 --workers=2 --worker-class=gthread --worker-tmp-dir=/dev/shm --timeout=120 --preload frappe.app:application
```

## Python RQ Worker

#### Direct

```sh
$ ../env/bin/opentelemetry-instrument \
    ../env/bin/python ../apps/frappe/frappe/utils/bench_helper.py \
    frappe worker
```

#### With Frappe-Bench and active python environment

Make sure we have `frappe-bench` installed in same environment

```sh
$ . ../env/bin/activate
$ opentelemetry-instrument bench worker
```

## Bench Schedular

#### Direct

```sh
$ ../env/bin/opentelemetry-instrument \
    ../env/bin/python ../apps/frappe/frappe/utils/bench_helper.py \
    frappe schedule
```

#### With Frappe-Bench and active python environment

Make sure we have `frappe-bench` installed in same environment

```sh
$ . ../env/bin/activate
$ opentelemetry-instrument bench schedule
```
