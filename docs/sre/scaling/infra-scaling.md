## Infra Scaling

- As the application is already running as set of containers or pods, it can be scaled by increasing the replica of running instance.
- Except for scheduler, everything can have multiple replicas.
- Scale MariaDB in case of DB performance required. Add BI tools to Read Replicas.
- Scale Gunicorn in case of API performance required.
- Scale Workers in case background jobs queue performance is required.
- In case of Kubernetes, use official helm chart replace with your custom values.yaml. In case helm is not used in your infrastructure then generate a `deploy.yaml` template using `helm template` command. Setup Horizontal Pod Autoscaler for on standard deployments and custom worker deployments as per the load.
- In case of other orchestrators the base requirements are:
    - Use Filesystem which supports symlinks to mount `sites` directory. Static assets under `sites/assets` have internal symlinks.
    - All pods need to connect to mariadb and redis services.
    - Run separate Jobs or Tasks for setting `common_site_config.json` and creating new site.
