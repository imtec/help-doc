## Continuous Integration

### Use pre-commit for code lint and format

Install pre-commit using pip.

Place example file `frappe-ci-cd/.pre-commit-config.yaml` at root of your repo.

Make changes to config file as per need.

Current pre-commit requires `.eslintrc.js` and `.prettierrc` files to configure the checks. All example files are available under `frappe-ci-cd` directory.

Following example for `python:3-alpine` image used in CI task. Working directory is your root of your app repo.

```shell
# Install dependencies
apk add -U git nodejs gcc musl-dev

# Create py env
python3 -m venv env

# Upgrade pip
./env/bin/pip install -U pip

# Install pre-commit
./env/bin/pip install pre-commit

# execute pre-commit run
./env/bin/pre-commit run --color=always --all-files
```

### Use frappe/bench container image to test app.

Sample compose.yaml for running tests are placed under `frappe-ci-cd/frappe-tests.yaml`. Change the `command` for `tests` service.

Execute following command to run tests:

```shell
# Create and start services
docker compose -p frappe-tests -f frappe-ci-cd/frappe-tests.yaml create
docker compose -p frappe-tests -f frappe-ci-cd/frappe-tests.yaml start mariadb
docker compose -p frappe-tests -f frappe-ci-cd/frappe-tests.yaml start redis-cache
docker compose -p frappe-tests -f frappe-ci-cd/frappe-tests.yaml start redis-queue
docker compose -p frappe-tests -f frappe-ci-cd/frappe-tests.yaml start redis-socketio

# Run tests
docker compose -p frappe-tests -f frappe-ci-cd/frappe-tests.yaml run tests

# Stop services
docker compose -p frappe-tests -f frappe-ci-cd/frappe-tests.yaml stop

# Clean up
docker compose -p frappe-tests -f frappe-ci-cd/frappe-tests.yaml rm -f
docker volume prune -f
docker network prune -f
```

### Release individual app

Sample release script is under `frappe-ci-cd/release.py`. Place it in the root of your repo and execute to create release. It needs `GitPython` and `semantic-version` present under environment.

The release script bumps up the `__version__` string in `__init__.py`, adds git tag and `git push` with tags on specified `--remote` branch. If remote is not specified it will read remote from git and provide selection for user to input remote. Pass `--remote` in case of unattended execution.

```shell
# Create py env
python3 -m venv env

# Upgrade pip
./env/bin/pip install -U pip

# Install GitPython
./env/bin/pip install GitPython

# Install semantic-version
./env/bin/pip install semantic-version

./env/bin/python release.py --help

usage: release.py [-h] [-d] [-j | -n | -p]

optional arguments:
  -h, --help            show this help message and exit
  -d, --dry-run         DO NOT make changes
  -r REMOTE, --remote REMOTE
                        git remote to push release on
  -j, --major           Release Major Version
  -n, --minor           Release Minor Version
  -p, --patch           Release Patch Version
```
