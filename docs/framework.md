## Frappe Framework

- More than decade of history. Containers were not commonly used.
- Bench was designed for multi-tenant isolation of environment
- Metadata driven. Builds on the concept of DocType.

### Low code

- Useful for cases where development to be done with minimum to no coding requirements.
- Useful for scenarios where on-the-fly changes are needed to be done by the domain expert team.
- Ease of entity modeling for functional team members.
- Creating a DocType creates full stack components for it. DB table, list view, form view, ReST API, Role Permissions.

### Full Stack

- It provides full stack tools from backend to frontend development.
- It provides advanced development tools using python and js programming to build backend or frontend.
- It is used to extend the application beyond low-code/no-code features.

### Architecture

![Framework](/assets/frappe-framework.png)

**Bench**

Bench is the environment that serves the frappe framework sites. It consists of following components:

- Python / Gunicorn / ReST API
- Python RQ Background Workers
- Python Scheduler Process
- Assets built using NodeJS frontend development tools
- SocketIO served using NodeJS backend
- Nginx is used as part of application. Not to be confused with Load Balancer nginx. It is used for following:
  - Reverse Proxy Gunicorn / ReST API
  - Reverse Proxy SocketIO
  - Serve Static Assets
  - Serve user uploaded files.
  - Any custom configuration.

**App**

- Python package that will be installed in the bench environment
- Custom JS / CSS static asset builds
- Any additional processes using any runtime.

**Site**

- Database
- Installs some or all of the apps available on bench
- Faces downtime during migration
- One bench can host multiple sites.
