
## Testing and Maintaining Code Sanity

For maintaining a level of control over developers we have implemented following code sanity checks on our end which could be generally followed using `Pre-Commit`.

#### `pre-commit-hooks`

- Trailing white spaces
- Large file checks
- YAML format
- End of file checks
- etc

#### `pyupgrade`

- Upgrade syntax
- Python modernize
- etc

#### `black`

- Code format
- Code style
- etc

#### `flake8`
- Linting
- Enforce standards
- Un-used variables
- Un-used imports
- etc

#### `isort`
- import sorting
- grouping imports
- etc

#### `mirrors-prettier`
- prettier
- code readability

#### `codespell`
- spell checks

#### `mirrors-eslint`
- eslint
- code consistency
- code quality
- etc


### For adding code sanity to a new project

You would need to install `pre-commit`

Virtual env
```sh
# create a virtual environ meant
python3 -m venv env

# use it
. ./env/bin/activate

# install pre-commit
pip install pre-commit
```

Globally
```sh
pip install pre-commit
```

Create a file `pre-commit-config.yaml` and `.prettierrc` on root of your project.

You can create your own or use following configuration [pre-commit-config](https://gitlab.com/castlecraft/frappe_utils/-/blob/main/.pre-commit-config.yaml) & [.prettierrc](https://gitlab.com/castlecraft/frappe_utils/-/blob/main/.prettierrc)


#### Run `pre-commit` for all files

```sh
pre-commit run --color=always --all-files
```

    Note: If you have a project with lot of files kindly progressively fix changes and don't try to fix all test at once.

    Comment all tests from config and uncomment one with every PR.
