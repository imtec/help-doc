# Release Management

### Developer Requirement

![multi-app-releases](/assets/multi-app-releases.png)

- Developer Alice wants to release a specific code state of the `Agency` app in the bench.
- Developer Bob wants to release a specific code state of the `Booking` app in the bench.
- Developer Charlie wants to release a specific code state of the `Contracts` app in the bench.

### Problem

- As the final bench is built with branches for specific environment, All the code travels in sync using branches.
- Path of the code bench can assume `predev` -> `functional` -> `predev` -> `dev` -> `staging` -> `main`.

### Proposed Solutions

- **git tag** the version and use version tags instead of branches to pull code. If tags are auto-created and git admins can cleanup tags then tag is not under assured control of infrastructure team.
    - Simple to setup and use.
    - Use it in case you trust the tags are not going to be deleted any time.
- **Archive and push to object storage** the version of app as a build pipeline. It will create a tar.gz archive of the app, add a build / version / tag to it and push it to S3 or any assets storage like how github releases do e.g. https://github.com/wkhtmltopdf/packaging/releases. Use these archives during builds and `COPY` them into image. `RUN bench setup requirements && bench build --production` will set it up as part of bench image.
    - Use it when you cannot trust git tags.
    - It is difficult to setup and needs an storage location or an object storage like S3.
- **Package version into container image** and push it to container registry. These images will be use to archive versions and then retrieve in `Dockerfile` using [`COPY --from=<image-name>`](https://docs.docker.com/engine/reference/builder/#copy). `RUN bench setup requirements && bench build --production` will set it up as part of bench image.
    - Use it when you cannot trust git tags and additional object storage cannot be provisioned.
    - Difficult compared to git tags. It is similar to archiving on object storage.
