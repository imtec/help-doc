## Custom version specific to doctype

For cases where segregation of doctype version into its own doctype specific table is needed.

> Something similar to **sharding** existing version table by doctype is needed.

Existing frappe document could be overwritten with doc specific versioning to achieve sharding.


## Frappe Version Sharding

**Existing**

![image.png](../../assets/frappe-version-doctype.png)

**Sharding**

![image.png](../../assets/frappe-version-doctype-sharding.png)
