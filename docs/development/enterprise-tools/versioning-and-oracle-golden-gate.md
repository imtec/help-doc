
## Seen/Viewed with Oracle golden gate

With issue over Seen/Viewed triggering hooks on `Oracle golden gate` being an issue as it will trigger hooks that will publish things to kafka.

Proposed potential resolution.

1. Handle something on Oracle golden gate Kafka policy if possible.
2. Overwrite frappe layer with version like functionality


**Handle Oracle golden gate**

If its possible to handle either entrypoint or the hook to exclude entries with seen/viewed column update we could handle it that way and potentially not touch framework.

**Overwrite frappe layer**

If we don't have an option we could overwrite frappe document.py and modify layer which create seen/viewed such that seen and viewed are appended as a part of separate table similar to how versions work.

Following will need to be implemented on the code side and would require some research to figure out scope of work and if/any challenges involved around the same.
