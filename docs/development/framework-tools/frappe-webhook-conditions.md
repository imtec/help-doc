
## Frappe Webhooks nested table conditions

Consider a case where we have an invoice

```JSON
{
    "name" : "Invoice 1",
    "customer" : "CEPL",
    "warehouse": "Mumbai",
    "items" : [
        {
            "name": "Apple",
            "qty": 3,
            "status": "ripe"
        },
        {
            "name": "Apple",
            "qty": 3,
            "status": "fresh"
        }
    ]
}
```

If we have a case where we would like to trigger notification only when an apple is `fresh` we can achieve it by something like this


```python
(not not ([item for item in data.get("items") if item.get("status") == "fresh"]))
```

### Paste to try out in frappe console

Internally frappe uses safe_eval for webhook conditions

```python
data = {
    "name" : "Invoice 1",
    "customer" : "CEPL",
    "warehouse": "Mumbai",
    "items" : [
        {
            "name": "Apple",
            "qty": 3,
            "status": "ripe"
        },
        {
            "name": "Apple",
            "qty": 3,
            "status": "fresh"
        }
    ]
}

frappe.safe_eval(
    'not not ([item for item in data.get("items") if item.get("status") == "fresh"])',
    {"data":data}
)
```
