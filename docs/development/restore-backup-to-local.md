## For restoring frappe backups


#### Generate a backup and download from Frappe Desk

> Note: Backups restoring to file are async, please wait for the backup file to complete.

![Screenshot_from_2023-10-05_16-58-30](https://gitlab.com/castlecraft/frappe_utils/-/wikis/uploads/e1bf6e9d8220ca143fc9b1e3ae275bf8/Screenshot_from_2023-10-05_16-58-30.png)

#### Restore downloaded backup to SQL

```sh
# login to your local mariadb
mysql -u root -p your_pass

# NOTE: If you have backed-up before make sure to delete database with the same name.

# Restore backup to db
mysql -u  root -p your_pass <  YOUR_BACKUP_FILE_PATH.SQL;
```

#### Remove apps that are available locally.

> As backed-up data may be from a site which has additional apps compared to local frappe these apps would clash, you need to remove them as follows.

```sh
bench --site YOUR_SITE remove-from-installed-apps YOUR_APP_NAME
```

#### Remove SSO provider

> SSO provider from backup will not work out of the box as file config are local hence either you can update your `site_config` with updated encryption key, or remove SSO from local.

you can disable SSO from SQL.

```sh
# login to your local mariadb
mysql -u root -p your_pass

# disable SSO
update `tabSocial Login Key` set  enable_social_login = 0 ;
```

You should be able to access frappe UI!

**Creating new SSO will regenerate new keys.**

#### For access to administrator or any system user reset following credentials

In frappe the `__Auth` table has encrypted passwords, we can set all passwords to admin by following query.

```sql
SET SQL_SAFE_UPDATES = 0;

UPDATE `__Auth`
SET password = "$pbkdf2-sha256$29000$TWntnXOudU4p5dx7L4VwDg$0/1qjbAJIhhRkIagC5T5hgEJ0d.bDE16stjsrj.rtwA"   /* Encrypted hash for `admin` */
WHERE fieldname = "password";

SET SQL_SAFE_UPDATES = 1;
```

> You should be abe to login with any user with password `admin`

## Using maria-backup snapshots.

Following needs to be executed on server to take snapshot of mariadb:

```shell
mariabackup --backup --user=root --password=123 --databases="_7e64c01a29e2a82d" --stream=xbstream | gzip > ./site.name.com/private/backups/2023111120000_dump.xb.gz
```

This will make the `2023111120000_dump.xb.gz` file available in "Download Backups".

To restore on local server:

Stop mariadb first and execute following to extract files

```shell
mkdir -p /tmp/dump
cd /tmp/dump
gunzip -c ${HOME}/Downloads/2023111120000_dump.xb.gz | mbstream -x
```

Prepare for restore:

```shell
mariabackup --prepare --target-dir=/tmp/dump
```

Restore with rsync:

```shell
# Worked from host machine
sudo rsync -avrP /tmp/dump/ /var/lib/mysql
chown -R mysql:mysql /var/lib/mysql
```

Start mariadb
