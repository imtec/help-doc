## Single App Migration

```shell
bench --site {site} migrate-app --force {apps}
```

Note:

- replace `{site}` with appropriate value or `all` keyword to run command on all sites.
- `{app}/__init__.py` should have a flag `is_set_to_migrate` for app to be migrated or `--force` is used to force migrate the app.
- `{apps}` is a space separated string. e.g. `frappe` or `frappe frappe_utils imtec`.
