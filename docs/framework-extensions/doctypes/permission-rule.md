
## Permission Rule

Let's consider DocType `Invoice`, `Customer` and `Warehouse`

![Doctype image](/assets/permission-rule/user-perm-example-diagram.png)

### Let's consider we would like to achieve following permission control on `Invoice`

1. Only `read` permission for `customer 1`,  `warehouse 2`
2. Only `create` permission for `customer 1`,  `warehouse 2`
3. Only `write` permission for `customer 1`,  `warehouse 2`

#### 1. For achieving `read` permission for `customer 1`,  `warehouse 2` we need to following steps

Create new permission rule with permission as required with following inputs

- **For Doctype**: Choose DocType for which you want to add permission rule. `Invoice`

- **Name**: Name permission rule. `rule 1`

- **Permission Rule Links**: Add link document type and link name field from target DocType that you want to include in permission rule links. `customer 1`, `warehouse 2`

- **Granular Permission**: Choose granular permission for Permission Rule Links (Defaults to select and read) `read`


##### Creating permission rule

![Permission rule image](/assets/permission-rule/read-user-permission-rule.png)

**Assigning permission rule to user**

- **User**: Select user to assign permission rule. `anonymous@gmail.com`

- **For Doctype**: Select Doctype for which permission rule is created. `Invoice`

- **Permission Rule**: Add permission rule. `rule 1`

<br>

![Assign permission rule image](/assets/permission-rule/read-assign-permission-rule.png)

User can `read` for customer 1 and warehouse 2

![Read image](/assets/permission-rule/can-read.png)

#### If user tries to create, write, and delete, it will throw an error

![Delete error message image](/assets/permission-rule/no-granular-delete-permission.png)

<hr>

#### 2. For achieving `create` permission for `customer 1`,  `warehouse 2` we need to following steps

Create new permission rule with permission as required with following inputs

##### Creating permission rule

![Permission Rule image](/assets/permission-rule/create-user-permission-rule.png)

**Assigning permission rule to user**

![Assign permission rule image](/assets/permission-rule/create-assign-user-permission-rule.png)

User can `create` for customer 1 and warehouse 2

![Create image](/assets/permission-rule/can-create.png)

<hr>

#### 3. For achieving `write` permission for `customer 1`,  `warehouse 2` we need to following steps

Create new permission rule with permission as required with following inputs

##### Creating permission rule

![Permission Rule image](/assets/permission-rule/write-user-permission-rule.png)

**Assigning permission rule to user**

![Assign permission rule image](/assets/permission-rule/write-assign-user-permission-rule.png)

User can `update` for customer 1 and warehouse 2

![Write image](/assets/permission-rule/can-write.png)
