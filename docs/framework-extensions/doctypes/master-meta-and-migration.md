

# Master Meta

Master Meta allow for exporting the master Doctype data to json.

> Note : The exported json is saved inside the app selected in Exported App field.

### Fields
- **Meta Doctype** : We have Master Meta doctype where a master doctype could be declared and synced.

- **Status** : Shows if the Master Meta is already exported/synced with the latest master doctype field.
> Note : <br>
        <br>At the time of creation the status is default set to desynced.<br>
        <br>If the master docType is updated after sync and auto sync is not turned on then status changes to desynced and Sync Fixture feature is available.

- **Auto Sync Fixture** : Option to auto export/sync the master meta every time docType is updated.

- **Exported App** : This field allows you to select for which app does the sync/export happen.

### Exclude Field
- **Exclude Update** : Fields that should be excluded while syncing/exporting.
- **Exclude Export** : Fields that should be excluded while importing.

#### Creating Master Meta
![Meta Master](/assets/meta-master/master-meta-form.png)


#### After Save message of Master Meta
![Meta Master Save Message](/assets/meta-master/master-meta-form-save-message.png)

> Note : Add the related docType to the master DocType, should their data to also be synced.


# Sync Masters

## Command

Command can be used to sync master as per latest synced state

```shell
bench --site {site} sync-masters
```

Note:

- replace `{site}` with appropriate value or `all` keyword to run command on all sites.

## API

Sync can also be triggered from APIs by a `system-manager` as follows


```shell
curl -X POST http://{{FRAPPE_SITE}}/api/method/frappe_utils.commands.site.sync_masters_api \
    -H 'Content-Type: application/json' \
    -H 'Authorization: Basic {{TOKEN}}==' \
    -d '{
        "sites" : ["site1.localhost"],
        "force": false,
        "verbose": false,
        "profile": false
    }'
```
