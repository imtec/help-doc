## DocField Master

DocField Master is to have master definitions and is a controlled version of Data Element. It outlines the metadata(properties) that define on Doctype Field   and can be used to standardize field-level properties for reference across other DocTypes.

### Creating new DocField Master

**Module**: A "module" refers to the categorization or grouping under which this new field will be organized.

**Doctype Element**: A 'DocType Element' is a table consisting of:

-  **Field Property**: containing the metadata (properties) of the field.
-  **Field Value**: holding the actual value of the field.
-  **Immutable**: its for enforcing a field to be not changeable.

    *Example*

    If you set a length of 20 and make a field immutable, anyone consuming your master data cannot change the value of the length for this field.


![Creating New DocField Master](/assets/docfield_master_new_doctype.png)

> Note: 'fieldname' and 'fieldtype' are required field_property

After creation, a new DocField Master is initially in a pending state. Once approved, it becomes ready for use.

You can also make the existing field as Docfield Master, refer the below link

[direct creation of Docfield Master](https://help-doc-imtec-b249c70d8b6d99bd095c0ef03e4a3115a94f5.gitlab.io/feature-and-extensions/doctypes/doctype-editor/#is_docfield_master-checkbox)
