## Paginated Child Tables

For listing `Email Group` linked to a `Newsletter` **imtec** you can query it as belows

**_API_**

```
https://<YOUR_FRAPPE_SITE>/api/method/frappe_utils.common.crud.get_paginated_list?
    doctype=Newsletter Email Group
    fields=["*"]
    filters=[
        [
            "parent","=","imtec"
        ]
    ]
    limit_start=0
    limit_page_length=5
```

**_CURL_**

```
curl 'https://<YOUR_FRAPPE_SITE>/api/method/frappe_utils.common.crud.get_paginated_list?doctype=Newsletter%20Email%20Group&fields=%5B%22*%22%5D&filters=%5B%5B%22parent%22%2C%22%3D%22%2C%22castlecraft%22%5D%5D&limit_start=0&limit_page_length=1'
```

All filters used in [frappe listing API](https://frappeframework.com/docs/user/en/api/rest) can be used here.
