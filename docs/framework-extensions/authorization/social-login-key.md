
## Social Login Key

Use built-in [Social Login Key](https://frappeframework.com/docs/user/en/guides/integration/social_login_key) to allow frappe app users to login with existing OAuth2/OIDC providers.
