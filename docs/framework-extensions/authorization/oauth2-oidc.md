
# OAuth2 OIDC

Use [castlecraft](https://github.com/castlecraft/cfe) app to override auth. It allows you to accept any OAuth 2/OpenID `access_token` or `id_token` in you frappe app.

The `userinfo` endpoint or the `id_token` must have a claim with email address of the user. That user will be set for incoming request with appropriate `Authorization` header.
